public with sharing class sObjectUtils {

	public class QueryBuilder {
		protected List<QueryCommand> commands;
		private Boolean NoSelectWord;
		private Boolean isWhereAdded;

		public QueryBuilder() {
			commands = new List<QueryCommand>();
			commands.add(new PureTextCommand('SELECT'));
			NoSelectWord = false;
			isWhereAdded = false;
		}

		public QueryBuilder(Boolean no_select) {
			commands = new List<QueryCommand>();
			commands.add(new PureTextCommand('SELECT'));
			this.NoSelectWord = no_select;
			this.isWhereAdded = false;
		}
		/*========================== query builder methods ================*/
		public String asStr() {
			List<String> command_results = new List<String>();

			if (!NoSelectWord)
				command_results.add(commands.get(0).exec(null, null));

			for (Integer i = 1; i < commands.size(); i++) {
				QueryCommand prev = commands.get(i-1);
				QueryCommand next = i < commands.size() - 1 ? commands.get(i+1) : null;
				if (next != null && next.isPrevSupress())
					continue;
				QueryCommand command = commands.get(i);
				command_results.add(command.exec(prev, next));
			}
			return String.join(command_results, ' ');
		}

		public QueryBuilder addCommand(QueryCommand command) {
			commands.add(command);
			return this;
		}

		public QueryBuilder fields(String fields_str) {
			return addCommand(new FieldsString(fields_str));
		}

		public QueryBuilder fields(Iterable<String> fields_list) {
			return addCommand(new FieldsList(fields_list));
		}

		public QueryBuilder fields(Set<String> fields_set) {
			List<String> fields_list = new List<String>(fields_set);
			return addCommand(new FieldsList(fields_list));
		}

		public QueryBuilder fields(List<Schema.FieldSetMember> field_set) {
			return addCommand(new FieldsSet(field_set));
		}

		public QueryBuilder fields(Schema.FieldSet field_set) {
			return fields(field_set.getFields());
		}

		public QueryBuilder fields(List<Schema.FieldSetMember> field_set,
									Iterable<String> all_time_fields)
		{
			return addCommand(new FieldsSetFieldList(field_set, all_time_fields));
		}

		public QueryBuilder fields(QueryBuilder inner_query) {
			return addCommand(new InnerFields(inner_query, true));
		}

		public QueryBuilder InBrackets(QueryBuilder inner_query) {
			return addCommand(new InnerFields(inner_query, false));
		}

		public QueryBuilder innerQ() {
			return new QueryBuilder();
		}

		public QueryBuilder innerF() {
			return new QueryBuilder(true);
		}

		public QueryBuilder var(String field_name) {
			return addCommand(new PureTextCommand(field_name));
		}

		public QueryBuilder whereField(String field_name) {
			PureTextCommand textCommand = new PureTextCommand(field_name);
			if (!isWhereAdded)
				WhereC();

			return var(field_name);
		}

		public QueryBuilder notNull() {
			return addCommand(new PureTextCommand('!= NULL'));
		}

		public QueryBuilder eq(Object obj) {
			return addCommand(new PureTextCommand('= ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder neq(Object obj) {
			return addCommand(new PureTextCommand('!= ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder gt(Object obj) {
			return addCommand(new PureTextCommand('> ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder gte(Object obj) {
			return addCommand(new PureTextCommand('>= ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder lt(Object obj) {
			return addCommand(new PureTextCommand('< ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder lte(Object obj) {
			return addCommand(new PureTextCommand('<= ' + sObjectUtils.ObjectToSoqlVar(obj)));
		}

		public QueryBuilder inC() {
			return addCommand(new PureTextCommand('IN'));
		}

		public QueryBuilder inC(Iterable<Object> values) {
			return addCommand(new PureTextCommand('IN ' + sObjectUtils.ListToSoqlInForm(values)));
		}

		public QueryBuilder inC(List<String> values) {
			return inC((List<Object>)values);
		}

		public QueryBuilder inC(List<Id> values) {
			return inC((List<Object>)values);
		}

		public QueryBuilder inC(Set<Object> values) {
			List<Object> values_list = new List<Object>(values);
			return addCommand(new PureTextCommand('IN ' + sObjectUtils.ListToSoqlInForm(values_list)));
		}

		public QueryBuilder inC(Set<String> values) {
			List<String> values_list = new List<String>(values);
			return addCommand(new PureTextCommand('IN ' + sObjectUtils.ListToSoqlInForm((List<Object>)values_list)));
		}

		public QueryBuilder inC(Set<Id> values) {
			List<Id> values_list = new List<Id>(values);
			return addCommand(new PureTextCommand('IN ' + sObjectUtils.ListToSoqlInForm((List<Object>)values_list)));
		}

		public QueryBuilder contains(String val) {
			return addCommand(new PureTextCommand('LIKE ' + sObjectUtils.ObjectToSoqlVar('%' + val + '%')));
		}

		public QueryBuilder fieldsContains(String value, List<String> fields) {
			QueryBuilder brackets = innerF();
			for (Integer i = 0; i < fields.size(); i++) {
				brackets.var(fields.get(i)).contains(value);
				if (i < fields.size() - 1)
					brackets.orC();
			}
			InBrackets(brackets);
			return this;
		}

		public QueryBuilder likeC(String val) {
			return addCommand(new PureTextCommand('LIKE ' + sObjectUtils.ObjectToSoqlVar(val)));
		}

		public QueryBuilder between(Object min, Object max) {
			return addCommand(new BetweenCommand(min, max));
		}

		public QueryBuilder condition(ConditionWrapper cond) {
			cond.toSoqlCondition(this);
			return this;
		}

		public QueryBuilder andC() {
			return addCommand(new PureTextCommand('AND'));
		}

		public QueryBuilder andC(String val) {
			commands.add(new PureTextCommand('AND'));
			var(val);
			return this;
		}

		public QueryBuilder orC() {
			return addCommand(new PureTextCommand('OR'));
		}

		public QueryBuilder orC(String val) {
			commands.add(new PureTextCommand('OR'));
			var(val);
			return this;
		}

		public QueryBuilder FromC(String val) {
			return addCommand(new FromCommand(val));
		}

		public QueryBuilder WhereC() {
			PureTextCommand whereCommand = new PureTextCommand('WHERE');
			isWhereAdded = true;
			return addCommand(whereCommand);
		}

		public QueryBuilder WhereC(String val) {
			WhereC();
			return var(val);
		}

		public QueryBuilder GroupByC() {
			return addCommand(new PureTextCommand('GROUP BY'));
		}

		public QueryBuilder GroupByC(String val) {
			commands.add(new PureTextCommand('GROUP BY'));
			var(val);
			return this;
		}

		public QueryBuilder OrderByC() {
			return addCommand(new PureTextCommand('ORDER BY'));
		}

		public QueryBuilder OrderByC(String val) {
			commands.add(new PureTextCommand('ORDER BY'));
			var(val);
			return this;
		}

		public QueryBuilder limitC(Integer lim) {
			return addCommand(new PureTextCommand('LIMIT ' + String.valueOf(lim)));
		}

		public QueryBuilder offsetC(Integer off) {
			return addCommand(new PureTextCommand('OFFSET ' + String.valueOf(off)));
		}

		public QueryBuilder CloneQuery() {
			QueryBuilder result = new QueryBuilder(NoSelectWord);
			for (Integer i = 1; i < commands.size(); i++) {
				result.addCommand(commands.get(i));
			}
			return result;
		}

		public QueryBuilder addQuery(QueryBuilder query) {
			for (Integer i = 1; i < query.commands.size(); i++) {
				addCommand(query.commands.get(i));
			}
			return this;
		}
	}
	/* =================================== inner classes ============================ */
	public interface ConditionWrapper {
		void toSoqlCondition(QueryBuilder q);
	}

	public class CheckBoxBlock implements ConditionWrapper {
		public String Label { get; set; }
		public String FieldName { get; set; }
		public List<CheckBox> CheckBoxes { get; set; }

		public CheckBoxBlock(String label, String field_name) {
			this.Label = label;
			this.FieldName = field_name;
			CheckBoxes = new List<CheckBox>();
		}

		public CheckBoxBlock(String label, String field_name, List<SelectOption> options) {
			this.Label = label;
			this.FieldName = field_name;
			this.CheckBoxes = new List<CheckBox>();
			for (SelectOption option: options) {
				this.CheckBoxes.add(new CheckBox(option));
			}
		}

		public CheckBoxBlock(Schema.DescribeFieldResult field_result) {
			this.Label = field_result.getLabel();
			this.FieldName = field_result.getName();
			this.CheckBoxes = new List<CheckBox>();

			List<Schema.PicklistEntry> pickist_values = field_result.getPicklistValues();
			for (Schema.PicklistEntry value: pickist_values) {
				this.CheckBoxes.add(new CheckBox(value.getLabel(), value.getValue()));
			}
		}

		public Boolean isSelected() {
			for (CheckBox check: CheckBoxes) {
				if (check.isChecked)
					return true;
			}
			return false;
		}

		public void toSoqlCondition(QueryBuilder q) {
			List<Object> values = new List<Object>();
			for (CheckBox check: CheckBoxes) {
				if (check.isChecked)
					values.add(check.Value);
			}
			q.var(FieldName).inC(values);
		}
	}

	public class CheckBox {
		public Boolean isChecked { get; set; }
		public String Label { get; set; }
		public String Value { get; set; }

		public CheckBox(SelectOption option) {
			this.isChecked = false;
			this.Label = option.getLabel();
			this.Value = option.getValue();
		}

		public CheckBox(String label, String value) {
			this.isChecked = false;
			this.Label = label;
			this.value = value;
		}
	}


	private virtual class QueryCommand {
		public virtual String exec(QueryCommand prev, QueryCommand next) {
			return '';
		}

		public Boolean isWhereRequired = false;
		public Boolean isWhereCommand = false;

		public virtual Boolean isCommaRequired() {
			return false;
		}

		public virtual Boolean isPrevSupress() {
			return false;
		}
	}

	private class PureTextCommand extends QueryCommand {
		private String text;

		public PureTextCommand(String text) {
			this.text = text;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			return text;
		}
	}

	private class FromCommand extends QueryCommand {
		private String Val;

		public FromCommand(String val) {
			this.val = val;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			return 'FROM ' + val;
		}
	}

	private class FieldsString extends QueryCommand {
		private String fields_str;

		public FieldsString(String fields_str) {
			this.fields_str = fields_str;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			if (prev.isCommaRequired())
				return ', ' + this.fields_str;
			else
				return this.fields_str;
		}

		public override Boolean isCommaRequired() {
			return true;
		}
	}

	private class FieldsList extends QueryCommand {
		private Iterable<String> fieldList;

		public FieldsList(Iterable<String> field_list) {
			this.fieldList = field_list;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			String result = String.join(fieldList, ', ');

			if (!String.isEmpty(result.trim()) && prev.isCommaRequired())
				return ', ' + result;
			else
			 	return result;
		}

		public override Boolean isCommaRequired() {
			return true;
		}
	}

	private class BetweenCommand extends QueryCommand {
		private Object min;
		private Object max;

		public BetweenCommand(Object min, Object max) {
			this.min = min;
			this.max = max;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			String min_str = sObjectUtils.ObjectToSoqlVar(min);
			String max_str = sObjectUtils.ObjectToSoqlVar(max);
			String field_str = prev.exec(this, next);
			List<String> result = new List<String> { field_str, '>=', min_str, 'AND',
													 field_str, '<=', max_str};

			return '(' + String.join(result, ' ') + ')';
		}

		public override Boolean isPrevSupress() {
			return true;
		}
	}

	private class FieldsSet extends QueryCommand {
		private List<Schema.FieldSetMember> fieldSet;

		public FieldsSet(List<Schema.FieldSetMember> field_set) {
			this.fieldSet = field_set;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			List<String> field_list = new List<String>();
			for (Schema.FieldSetMember field_member: fieldSet) {
				field_list.add(field_member.getFieldPath());
			}

			String result = String.join(field_list, ', ');

			if (prev.isCommaRequired() && !String.isEmpty(result.trim()))
				return ', ' + result;
			else
			 	return result;
		}

		public override Boolean isCommaRequired() {
			return true;
		}
	}

	private class FieldsSetFieldList extends QueryCommand {
		private List<Schema.FieldSetMember> fieldSet;
		private Iterable<String> allTimeFields;

		public FieldsSetFieldList(List<Schema.FieldSetMember> field_set,
						 Iterable<String> all_time_fields)
		{
			this.fieldSet = field_set;
			this.allTimeFields = all_time_fields;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			Set<String> field_list = new Set<String>();
			for (Schema.FieldSetMember field_member: fieldSet) {
				field_list.add(field_member.getFieldPath());
			}

			Iterator<String> iter = allTimeFields.iterator();
			while (iter.hasNext()) {
				field_list.add(iter.next());
			}

			String result = String.join(new List<String>(field_list), ', ');

			if (prev.isCommaRequired() && !String.isEmpty(result.trim()))
				return ', ' + result;
			else
			 	return result;
		}

		public override Boolean isCommaRequired() {
			return true;
		}
	}

	private class InnerFields extends QueryCommand {
		private QueryBuilder InnerQuery;
		private Boolean isFields;

		public InnerFields(QueryBuilder inner_query, Boolean is_fields) {
			this.InnerQuery = inner_query;
			this.isFields = is_fields;
		}

		public override String exec(QueryCommand prev, QueryCommand next) {
			String result =  '(' + InnerQuery.asStr() + ')';
			if (prev.isCommaRequired())
				return ',' + result;
			else
				return result;
		}

		public override Boolean isCommaRequired() {
			return isFields;
		}
	}

	/* ======================= Static functions ==================*/
	public static String ListToSoqlInForm(Iterable<Object> objects) {
		List<String> result = new List<String>();
		Iterator<Object> iter = objects.iterator();
		while (iter.hasNext()) {
			result.add(ObjectToSoqlVar(iter.next()));
		}
		return '( ' + String.join(result, ', ') + ' )';
	}

	public static String ObjectToSoqlVar(Object obj) {
		if (obj == null)
			return 'NULL';

		if (obj instanceof String) {
			String result = (String)obj;
			return '\''  + String.escapeSingleQuotes(result) + '\'';
		}

		if (obj instanceof Date) {
			Date result = (Date)obj;
			return DateTime.newInstance(result, Time.newInstance(0, 0, 0, 0)).format('yyyy-MM-dd');
		}

		if (obj instanceof Datetime) {
			Datetime result = (DateTime)obj;
			return result.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
		}

		String result = String.valueOf(obj);
		return String.escapeSingleQuotes(result);
	}

	public static Map<Object, List<sObject>> formObjectListMap(String field_name, List<sObject> objects) {
		Map<Object, List<sObject>> result = new Map<Object, List<sObject>>();

		for (sObject obj: objects) {
			Object val = obj.get(field_name);
			if (!result.containsKey(val))
				result.put(val, new List<sObject>());
			result.get(val).add(obj);
		}
		return result;
	}

	public static void getFieldValuesList(String field_name,
										  List<sObject> objects,
										  Boolean add_nulls,
										  List<Object> result) {
		for (sObject obj: objects) {
			Object val = obj.get(field_name);
			if (val == null && !add_nulls)
				continue;
			result.add(val);
		}
	}
}