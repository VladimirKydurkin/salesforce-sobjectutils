@isTest
private class sObjectUtilsTest {
	@isTest static void testWhereAutoAdd() {
		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		query.fields('f1,f2,f3').FromC('something').whereField('f2').eq(3).andC().whereField('f3').eq(5);
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT f1,f2,f3 FROM something WHERE f2 = 3 AND f3 = 5');
	}

	@isTest static void testSetOfFields() {
		Set<String> stringSet = new Set<String>();
		stringSet.add('f1');
		stringSet.add('f2');
		stringSet.add('f3');

		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		query.fields(stringSet).FromC('something');
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT f1, f2, f3 FROM something');
	}

	@isTest static void testInValsSet() {
		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		Set<String> valuesSet = new Set<String>();
		valuesSet.add('1');
		valuesSet.add('2');
		valuesSet.add('3');
		query.fields(valuesSet).WhereC('f2').inC(valuesSet);
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT 1, 2, 3 WHERE f2 IN ( \'1\', \'2\', \'3\' )');
	}

	@isTest static void testFieldContains() {
		List<String> fields = new List<String> { 'f1', 'f2' };

		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		query.fieldsContains('test', fields);
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT (f1 LIKE \'%test%\' OR f2 LIKE \'%test%\')');
	}

	@isTest static void testBetweenCommand() {
		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		//you can use date too
		query.WhereC('field').between(10, 100);
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT WHERE (field >= 10 AND field <= 100)');
	}

	@isTest static void testBracketsInserting() {
		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		//select related list
		query.fields('Id').fields(query.innerQ().
			fields('f1, f2, f3').FromC('childObjects__r')
		)
		.FromC('someObject')
		.WhereC('parentId').inC().InBrackets(query.innerQ().fields('id').FromC('parents'))
		.orC().InBrackets(query.innerF().var('f1').gt(5).orC('f2').lt(3));
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT Id ,(SELECT f1, f2, f3 FROM childObjects__r) FROM someObject WHERE parentId IN (SELECT id FROM parents) OR (f1 > 5 OR f2 < 3)');
	}

	@isTest static void testConditionsCommands() {
		sObjectUtils.QueryBuilder query = new sObjectUtils.QueryBuilder();
		query.whereField('less').lt(1).orC('less_equal').lte('some_text');
		query.orC('greater').gt(10).andC('greater_equal').gte(Date.newInstance(2000, 2, 3));
		String result = query.asStr();
		System.debug(result);
		System.assertEquals(result, 'SELECT WHERE less < 1 OR less_equal <= \'some_text\' OR greater > 10 AND greater_equal >= 2000-02-03');
	}

	@isTest static void testFormObjectListMap() {
		List<Contact> contacts = new List<Contact>();
		for (Integer i = 0; i<21; i++) {
			String name_str = 'name ' + String.valueOf(Math.mod(i, 3));
			contacts.add(new Contact(FirstName = name_str));
		}
		Map<Object, List<Contact>> result = sObjectUtils.formObjectListMap('FirstName', contacts);
		System.assertEquals(3, result.keySet().size());
		System.assertEquals(7, result.get('name 0').size());
		System.assertEquals(7, result.get('name 1').size());
		System.assertEquals(7, result.get('name 2').size());
	}

	@isTest static void testGetFieldValuesList() {
		List<Contact> contacts = new List<Contact> {
			new Contact(FirstName = 'test1'),
			new Contact(FirstName = 'test2'),
			new Contact(FirstName = null)
		};
		List<String> results_with_null = new List<String>();
		sObjectUtils.getFieldValuesList('FirstName', contacts, true, results_with_null);
		System.assertEquals(3, results_with_null.size());
		List<String> results_without_null = new List<String>();
		sObjectUtils.getFieldValuesList('FirstName', contacts, false, results_without_null);
		System.assertEquals(2, results_without_null.size());
	}

		@isTest static void testBuilder() {
		List<String> field_list = new List<String> {
			'f5', 'f6', 'f7', 'f8', 'f9'
		};

		List<String> vals = new List<String> {
			'1', '2', '3'
		};

		sObjectUtils.QueryBuilder q = new sObjectUtils.QueryBuilder();
		q.fields('f1, f2, f3, f4');
		q.fields(field_list);
		q.fields(q.innerQ().fields('f1,f2,f3').FromC('Child__r'));
		q.FromC('Object__c');
		q.WhereC('f5').eq(4);
		q.orC().fields(q.innerF().var('f1').eq(4).andC('f2').eq(5));
		q.andC().var('f2').neq(Date.newInstance(2000, 1, 1));
		q.orC('f5').notNull();
		q.orC('f1').inC(vals);
		q.orC('f5').between(1, 5);
		q.orC('f8').gte(5);
		q.orC('f9').lte(0);
		q.orC('f4').contains('test');
		q.orc('f5').likeC('tte');
		q.orC('f1').gt(10);
		q.orC('f2').lt(2);
		q.orC('f4').eq(null);
		q.orC('f5').gt(Datetime.newInstance(123));
		q.orC().InBrackets(q.innerF().var('g2').gt(1).orC('g4').lt(2));

		sObjectUtils.CheckBoxBlock check_boxes = new sObjectUtils.CheckBoxBlock(Contact.LeadSource.getDescribe());
		q.andC().condition(check_boxes);
		List<SelectOption> options = new List<SelectOption> {
			new SelectOption('1', '1'),
			new SelectOption('2', '2'),
			new SelectOption('3', '3')
		};
		sObjectUtils.CheckBoxBlock check_boxes2 = new sObjectUtils.CheckBoxBlock('test label', 'Some_Field__c', options);
		System.assertEquals(false, check_boxes2.isSelected());
		check_boxes2.CheckBoxes.get(0).isChecked = true;
		System.assertEquals(true, check_boxes2.isSelected());
		q.orC().condition(check_boxes2);

		q.OrderByC('f2');
		q.GroupByC('f1, f2, f3, f4');
		String result = q.asStr();

		sObjectUtils.QueryBuilder q_clone = q.CloneQuery();
		System.assertEquals(q.asStr(), q_clone.asStr());
	}
}